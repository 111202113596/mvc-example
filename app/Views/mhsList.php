<html>

<head>
	<title>Native MVC Example</title>
	<link rel="stylesheet" href="/mvc-example/assets/css/bootstrap.css" />
	<script type="text/javascript" src="/mvc-example/assets/js/bootstrap.js"></script>
</head>

<body>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-4">&nbsp;</div>
			<div class="col-md-4">
				<h3>Data Mahasiswa</h3>
				<table class="table table-responsive table-bordered table-striped">
					<tr>
						<td>No</td>
						<td>NIM</td>
						<td>Nama</td>
						<td colspan="2" class="text-center">Aksi</td>
					</tr>
					<?php

					foreach ($rs as $mahasiswa => $list) {
						echo '<tr>
										<td>
											<a href="?act=tampil-data&i=' . $list['id'] . '">' . $list['id'] . '</a>
										</td>
										<td>' . $list['nim'] . '</td>
										<td>' . $list['nama'] . '</td>
										<td><a href="?act=edit-data&i=' . $list['id'] . '" class="btn btn-primary">Edit</a></td>
										<td><a href="?act=hapus&d=' . $list['id'] . '" class="btn btn-danger">Delete</a></td>
									</tr>';
					}

					?>
				</table>
				<!-- <a href="/mvc-example/?act=tampil-data" class="btn btn-danger">Kembali</a> -->
				<a href="/mvc-example/" class="btn btn-primary">Tambah Data</a>
			</div>
			<div class="col-md-4">&nbsp;</div>
		</div>
	</div>
</body>

</html>